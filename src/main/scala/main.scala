
object main extends App{

  trait Wealth{
    def amount:Int
    def power:Int
  }

  trait GreatHouse{

    def name: String
    def wealth:Wealth

  }


  trait MakeWildFire{

    this: Wealth=>

    def makeWildFire:Wealth= {
      val countP = power
      val countA = amount

        new Wealth{
          def power=countP+50
          def amount = countA


      }
    }
  }


  trait CallDragon{
    this: Wealth =>

    def callDragon: Wealth = {
      val countP = power
      val countA = amount

      new Wealth {
        def power = countP*2

        def amount = countA


      }
    }
  }


  trait BorrowMoney {
    this: Wealth =>

    def borrowMoney: Wealth = {
      val countP = power
      val countA = amount

      new Wealth {
         def power = countP

         def amount = countA + 20


      }
    }
  }

  case class Targaryen(wealth:Wealth) extends GreatHouse with MakeWildFire with BorrowMoney with Wealth{
      def amount = wealth.amount
      def power = wealth.power
      def name = "Targaryen"
  }

  case class Lannisters(wealth: Wealth) extends GreatHouse with BorrowMoney with CallDragon with Wealth {
    def amount = wealth.amount
    def power = wealth.power
    def name = "Lannisters"
  }

var targaryen = Targaryen(new Wealth{
  def amount = 50

  def power = 100
})

  var lannisters =Lannisters(new Wealth{
    def amount = 90

    def power = 60
  })

  // Стратегия Targaryen
  def Strategy1(greatHouse: Targaryen): Wealth =
    Targaryen.apply(greatHouse.makeWildFire).borrowMoney

  // Стратегия Lannisters
  def Strategy2(greatHouse: Lannisters): Wealth =
    Lannisters.apply(greatHouse.callDragon).borrowMoney

  // Targaryen атакует Lannisters
  def Attack1(targaryen: Targaryen, lannisters: Lannisters): Wealth ={
    //Сначала Lanni применяет свою стратегию
    val wealthLan = Lannisters.apply(Strategy2(lannisters))
  new Wealth{
    def amount=wealthLan.amount-targaryen.amount
    def power=wealthLan.power-targaryen.power
  }
  }

  // Lannisters атакует Targaryen
  def Attack2(targaryen: Targaryen, lannisters: Lannisters): Wealth = {

    //Сначала Targ применяет свою стратегию
    val wealthTarg = Targaryen.apply(Strategy1(targaryen))
    new Wealth {
      def amount = wealthTarg.amount - lannisters.amount

      def power = wealthTarg.power - lannisters.power
    }
  }



  class GameOfThrones(val clan1:Targaryen,val clan2:Lannisters,motion:Int) {

    def nextTurn(strategy1: Wealth, strategy2: Wealth): GameOfThrones = {

      motion match {

        case 4 => new GameOfThrones(clan1,Lannisters.apply(Attack1(clan1,clan2)),motion+1)
        case 5 => new GameOfThrones(Targaryen.apply(Attack2(clan1,clan2)),clan2,0)
        case x if x%2==0 => new GameOfThrones(Targaryen.apply(strategy1), clan2, motion+1)
        case x if x%2==1 => new GameOfThrones(clan1, Lannisters.apply(strategy2), motion+1)
      }


    }
  }

  var game = new GameOfThrones(targaryen,lannisters,0)

  for(i<-0 until 8){

    println(s"\n${i+1} Ход")
    game=game.nextTurn(Strategy1(game.clan1),Strategy2(game.clan2))
    targaryen=game.clan1
    lannisters=game.clan2
    println("\n"+targaryen.name)
    println(targaryen.wealth.amount)
    println(targaryen.wealth.power)

    println("\n"+lannisters.name)
    println(lannisters.wealth.amount)
    println(lannisters.wealth.power)
  }


}
